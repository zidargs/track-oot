import StyleVarSettingsHandler from "/script/util/StyleVarSettingsHandler.js";

// register a11y colors
new StyleVarSettingsHandler("detached_window_background", "page-background-color");
