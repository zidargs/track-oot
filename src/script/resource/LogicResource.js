import JSONCResource from "/emcJS/data/resource/file/JSONCResource.js";

export default await JSONCResource.get("/logic/vanilla.min.json");
