import Panel from "/emcJS/ui/layout/Panel.js";
import ItemsResource from "/GameTrackerJS/data/resource/ItemsResource.js";
import ItemGridInteractive from "/GameTrackerJS/ui/itemgrid/interactive/ItemGridInteractive.js";
import OptionsObserver from "/GameTrackerJS/util/observer/OptionsObserver.js";
import "/GameTrackerJS/ui/itemgrid/interactive/components/entries/Item.js";
import "/GameTrackerJS/ui/itemgrid/interactive/components/entries/ProgressiveItem.js";
import DungeonstateResource from "../../../resource/DungeonstateResource.js";
import "../itemgrid/components/RewardItem.js";
import "./components/DungeonReward.js";
import "./components/DungeonType.js";
import "./components/DungeonHint.js";
import STYLE from "./DungeonState.js.css" assert {type: "css"};
import EventTargetManager from "/emcJS/util/event/EventTargetManager.js";

function createItemText(text) {
    const el = document.createElement("DIV");
    el.classList.add("text");
    el.innerHTML = text;
    return el;
}

function createItemPlaceholder() {
    const el = document.createElement("DIV");
    el.classList.add("placeholder");
    return el;
}

class DungeonState extends Panel {

    #activeTypesOptionsObserver = null;

    #activeTypesOptionsObserverEventManager = new EventTargetManager();

    constructor() {
        super();
        STYLE.apply(this.shadowRoot);
        /* --- */
        const dungeonData = DungeonstateResource.get();
        for (const ref in dungeonData) {
            const dData = dungeonData[ref];
            this.shadowRoot.append(createRow(this, ref, dData));
        }
        this.#activeTypesOptionsObserverEventManager.set("change", (event) => {
            const {value} = event;
            this.#applyActiveTypeOption(value);
        });
    }

    connectedCallback() {
        this.setAttribute("data-fontmod", "items");
        // load active types
        const activeTypesOption = this.activeTypesOption;
        if (!activeTypesOption) {
            this.#activeTypesOptionsObserver = null;
            this.#activeTypesOptionsObserverEventManager.switchTarget(null);
            this.#switchActive(this.active);
        } else {
            this.#activeTypesOptionsObserver = new OptionsObserver(activeTypesOption);
            this.#activeTypesOptionsObserverEventManager.switchTarget(this.#activeTypesOptionsObserver);
            this.#applyActiveTypeOption(this.#activeTypesOptionsObserver.value);
        }
    }

    get active() {
        return this.getAttribute("active");
    }

    set active(val) {
        this.setAttribute("active", val);
    }

    get orientation() {
        return this.getAttribute("orientation");
    }

    set orientation(val) {
        this.setAttribute("orientation", val);
    }

    get activeTypesOption() {
        return this.getAttribute("activetypesoption");
    }

    set activeTypesOption(val) {
        this.setAttribute("activetypesoption", val);
    }

    static get i18nObservedAttributes() {
        return ["active", "activetypesoption"];
    }

    attributeChangedCallback(name, oldValue, newValue) {
        switch (name) {
            case "active": {
                if (oldValue != newValue) {
                    this.#switchActive(newValue);
                }
            } break;
            case "activetypesoption": {
                if (oldValue != newValue) {
                    const activeTypesOption = this.activeTypesOption;
                    if (!activeTypesOption) {
                        this.#activeTypesOptionsObserver = null;
                        this.#activeTypesOptionsObserverEventManager.switchTarget(null);
                        this.#switchActive(this.active);
                    } else {
                        this.#activeTypesOptionsObserver = new OptionsObserver(activeTypesOption);
                        this.#activeTypesOptionsObserverEventManager.switchTarget(this.#activeTypesOptionsObserver);
                        this.#applyActiveTypeOption(this.#activeTypesOptionsObserver.value);
                    }
                }
            } break;
        }
    }

    #applyActiveTypeOption(value) {
        if (!value) {
            this.#switchActive(this.active);
        } else {
            this.#switchActive(value);
        }
    }

    #switchActive(value) {
        if (!Array.isArray(value)) {
            if (typeof value === "string") {
                value = value.split(/,\s*/);
            } else {
                value = [];
            }
        }
        for (const el of this.shadowRoot.querySelectorAll("[type]")) {
            el.classList.add("inactive");
        }
        for (const i of value) {
            if (!i) {
                return;
            }
            for (const el of this.shadowRoot.querySelectorAll(`[type~=${i}]`)) {
                el.classList.remove("inactive");
            }
        }
    }

}

Panel.registerReference("dungeon-status", DungeonState);
customElements.define("ootrt-dungeonstate", DungeonState);

function createRow(target, ref, data) {
    const el = document.createElement("DIV");
    el.classList.add("item-row");
    el.classList.add("inactive");
    const types = [];

    const items = ItemsResource.get();

    //////////////////////////////////
    // title
    const title = createItemText(data.title);
    title.style.color = data.color;
    el.append(title);
    //////////////////////////////////
    // small key
    if (data.keys) {
        const itemData = items[data.keys];
        const itm = ItemGridInteractive.createItem(data.keys, itemData);
        itm.classList.add("inactive");
        itm.setAttribute("type", "key");
        el.append(itm);
        /* register */
        types.push("key");
    } else {
        const itm = createItemPlaceholder();
        itm.classList.add("inactive");
        itm.setAttribute("type", "key");
        el.append(itm);
    }
    //////////////////////////////////
    // boss key
    if (data.bosskey) {
        const itemData = items[data.bosskey];
        const itm = ItemGridInteractive.createItem(data.bosskey, itemData);
        itm.classList.add("inactive");
        itm.setAttribute("type", "bosskey");
        el.append(itm);
        /* register */
        types.push("bosskey");
    } else {
        const itm = createItemPlaceholder();
        itm.classList.add("inactive");
        itm.setAttribute("type", "bosskey");
        el.append(itm);
    }
    //////////////////////////////////
    // silver rupees
    if (typeof data.silver_rupees === "object") {
        const contentGrid = [];
        if (Array.isArray(data.silver_rupees)) {
            contentGrid.push(data.silver_rupees.map((e) => {
                return [
                    {
                        "type": "item",
                        "value": e,
                        "visible": true
                    }
                ];
            }));
        } else {
            for (const [label, entry] of Object.entries(data.silver_rupees)) {
                contentGrid.push([
                    {
                        "type": "text",
                        "value": label,
                        "visible": true,
                        "halign": "start",
                        "width": 60
                    }, {
                        "type": "item",
                        "value": entry,
                        "visible": true
                    }
                ]);
            }
        }
        const itm = ItemGridInteractive.createItemMirrorCollection(target, {
            icon: "/images/items/rupee_silver.png",
            halign: "end",
            valign: "end",
            contentGrid,
            counting: true
        });
        itm.classList.add("inactive");
        itm.setAttribute("type", "silver_rupees");
        el.append(itm);
        /* register */
        types.push("silver_rupees");
    } else {
        const itm = createItemPlaceholder();
        itm.classList.add("inactive");
        itm.setAttribute("type", "silver_rupees");
        el.append(itm);
    }
    //////////////////////////////////
    // map
    if (data.map) {
        const itemData = items[data.map];
        const itm = ItemGridInteractive.createItem(data.map, itemData);
        itm.classList.add("inactive");
        itm.setAttribute("type", "map");
        el.append(itm);
        /* register */
        types.push("map");
    } else {
        const itm = createItemPlaceholder();
        itm.classList.add("inactive");
        itm.setAttribute("type", "map");
        el.append(itm);
    }
    //////////////////////////////////
    // compass
    if (data.compass) {
        const itemData = items[data.compass];
        const itm = ItemGridInteractive.createItem(data.compass, itemData);
        itm.classList.add("inactive");
        itm.setAttribute("type", "compass");
        el.append(itm);
        /* register */
        types.push("compass");
    } else {
        const itm = createItemPlaceholder();
        itm.classList.add("inactive");
        itm.setAttribute("type", "compass");
        el.append(itm);
    }
    //////////////////////////////////
    // reward
    if (data.boss_reward) {
        const itm = document.createElement("ootrt-dungeonreward");
        itm.classList.add("dungeon-status");
        itm.classList.add("inactive");
        itm.setAttribute("type", "reward");
        itm.setAttribute("ref", ref);
        el.append(itm);
        /* register */
        types.push("reward");
    } else {
        const itm = createItemPlaceholder();
        itm.classList.add("inactive");
        itm.setAttribute("type", "reward");
        el.append(itm);
    }
    //////////////////////////////////
    // type
    if (data.hasmq) {
        const itm = document.createElement("ootrt-dungeontype");
        itm.classList.add("dungeon-status");
        itm.classList.add("inactive");
        itm.setAttribute("type", "type");
        itm.setAttribute("ref", ref);
        el.append(itm);
        /* register */
        types.push("type");
    } else {
        const itm = createItemPlaceholder();
        itm.classList.add("inactive");
        itm.setAttribute("type", "type");
        el.append(itm);
    }
    //////////////////////////////////
    // hint
    if (data.hint) {
        const itm = document.createElement("ootrt-dungeonhint");
        itm.classList.add("dungeon-status");
        itm.classList.add("inactive");
        itm.setAttribute("type", "hint");
        itm.setAttribute("ref", ref);
        el.append(itm);
        /* register */
        types.push("hint");
    } else {
        const itm = createItemPlaceholder();
        itm.classList.add("inactive");
        itm.setAttribute("type", "hint");
        el.append(itm);
    }

    el.setAttribute("type", types.join(" "));

    return el;
}
